#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/CommandTOL.h>

#define DEBUG 1

#ifdef DEBUG
int test_data[] = {266, 249, 266, 200, -1, 255, 200, 255, 233, 228, 233, -1, 200, 233, 200, 277, -1, 200, 257, 268, 257, 268, 248, -1, 256, 248, 256, 261, 208, 261, -1, 208, 257, 309, 257, 309, 249, -1};
#endif

mavros_msgs::State current_state;
void state_cb(const mavros_msgs::State::ConstPtr& msg){
    current_state = *msg;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "offb_node");
    ros::NodeHandle nh;

    // Set up ROS publishers, subscribers and service clients
    ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State>
            ("mavros/state", 10, state_cb);
    ros::Publisher local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>
            ("mavros/setpoint_position/local", 10);
    ros::ServiceClient arming_client = nh.serviceClient<mavros_msgs::CommandBool>
            ("mavros/cmd/arming");
    ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>
            ("mavros/set_mode");
    ros::ServiceClient takeoff_client = nh.serviceClient<mavros_msgs::CommandTOL>
            ("mavros/cmd/takeoff");
    // TODO: Add other required topics/services

    // The setpoint publishing rate MUST be faster than 2Hz
    ros::Rate rate(20.0);

    // Wait for MAVROS SITL connection
    while(ros::ok() && !current_state.connected){
        ros::spinOnce();
        rate.sleep();
    }


    // geometry_msgs::PoseStamped desired_pose;
    // desired_pose.pose.position.x = 2;
    // desired_pose.pose.position.y = 0;
    // desired_pose.pose.position.z = 2;

    // mavros_msgs::CommandTOL takeoff_request;
    // takeoff_request.request.altitude = 1.5;

    mavros_msgs::SetMode guided_set_mode;
    guided_set_mode.request.custom_mode = "GUIDED";

    // mavros_msgs::CommandBool arm_cmd;
    // arm_cmd.request.value = true;

    set_mode_client.call(guided_set_mode);
    ROS_INFO("Waiting for GUIDED mode");
    while(ros::ok() && !current_state.guided){
        ros::spinOnce();
        rate.sleep();
    }
    ros::Duration(1).sleep();

    // TODO: Arm and Take Off

    ROS_INFO("Sending position command");

    while (ros::ok())
    {
        // TODO: Implement position controller and mission commands here

        //local_pos_pub.publish(desired_pose);
        ros::spinOnce();
        rate.sleep();
    }

    return 0;
}

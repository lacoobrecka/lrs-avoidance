#!/bin/bash

#terminator -l LRS



terminator --title="gazebo" -e "bash -c './convert_csv_to_yaml.sh; source ~/fei_lrs_gazebo/devel/setup.bash; source ~/fei_lrs_gazebo/src/fei_lrs_gazebo/setup.bash; cd ~/fei_lrs_gazebo/; roslaunch fei_lrs_gazebo fei_lrs_world.launch verbose:=true;' bash" &

terminator --title="ardupilot" -e "bash -c 'cd ~/ardupilot/ArduCopter; sleep 10 && sim_vehicle.py -f gazebo-iris --console'; bash" &

terminator --title="mavros" -e "source ~/fei_lrs_gazebo/devel/setup.bash; source ~/fei_lrs_gazebo/src/fei_lrs_gazebo/setup.bash; echo 'start this after +-1min: roslaunch fei_lrs_gazebo fei_lrs_mavros.launch';  bash" &

terminator --title="drone-launch" -e "source ~/fei_lrs_gazebo/devel/setup.bash; source ~/fei_lrs_gazebo/src/fei_lrs_gazebo/setup.bash; echo 'start this after +-2min after MAVROS: roslaunch drone_control drone_control.launch'; bash" &




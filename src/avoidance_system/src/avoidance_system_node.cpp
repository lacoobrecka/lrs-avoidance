#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sensor_msgs/point_cloud2_iterator.h>
#include "sensor_msgs/PointCloud2.h"

#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Pose.h"
#include <mavros_msgs/State.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/PositionTarget.h>
#include <memory>
#include <chrono>
#include <mutex>
#include <tf/transform_datatypes.h>

// main stuff
int cnt = 0;
ros::Subscriber sub;

bool OBSTACLE = false;

void callback_pointcloud(sensor_msgs::PointCloud2ConstPtr const& msg)
{
  // std::cout << "Starting callback!..." << std::endl;
  
  float obstacle_distance = 0.75;
  int all_obstacle_points = 0;
  int all_points = 0;
  float nearest = 10000.0;
  for (sensor_msgs::PointCloud2ConstIterator<float> it(*msg, "x"); it != it.end(); ++it) {
    if (!std::isnan(it[0])) 
    {
      
      // it[2] - os smerom pred robota
      // it[0] - os smerom doprava od robota
      // it[1] - os smerom dolu od robota
      all_points++;
      if (abs(it[2]) < 1.0 && abs(it[0]) < 0.3 && abs(it[1]) < 0.3)
        all_obstacle_points++;

      // std::cout << "X: " << it[0] << ", Y: " << it[1] << ", Z: " << it[2] << std::endl;
      // double dist = sqrt(pow(it[0], 2) + pow(it[1], 2));
      // if (dist > 1.0 && dist < 2.0)
      // {
      //   // std::cout << dist << std::endl;
      //   // all_points++;
      //   // if (dist < obstacle_distance)
      //   //   all_obstacle_points++;
      // }      
    }
  }

  double percentage_of_obstacle_points = all_obstacle_points != 0 ? float(all_obstacle_points) / float(all_points) : 0.0;
  std::cout << "Percentage of obstacle points: " << percentage_of_obstacle_points << "  --  ";
  if (percentage_of_obstacle_points > 0.1)
  {
    std::cout << "Obstacle!" << std::endl;
    OBSTACLE = true;
  }
  else
  {
    std::cout << "Free!" << std::endl;
    OBSTACLE = false;
  }
    
  // std::cout << "----------------------" << std::endl;

  // std::cout << "Ending callback!..." << std::endl;
  // sub.shutdown(); cnt = 1;
  // std::cout << "Callback ended!..." << std::endl;
}

// navigation stuff
struct XYZ_pos
{
    double x;
    double y;
    double z;
};

ros::Subscriber points_sub, odom_sub, state_sub;
ros::ServiceClient set_mode_client, arming_client, takeoff_client, land_client;
ros::Publisher local_pose_publisher;
mavros_msgs::SetMode guided_set_mode;
mavros_msgs::State current_state;
mavros_msgs::CommandBool arm_cmd;
mavros_msgs::CommandTOL takeoff_request, land_request;
geometry_msgs::Pose pose;
geometry_msgs::Pose tmp_pose;
mavros_msgs::PositionTarget target_position;

std::mutex mutex_;

void getOdometryCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  {
    std::lock_guard<std::mutex> guard( mutex_ );
    pose = msg->pose.pose;
  }
}

void setGuidedMode()
{
    while (true) 
    {
        {
            std::lock_guard<std::mutex> guard(mutex_);
            tmp_pose = pose;
        }
        if(tmp_pose.position.z > 0.05)
            ros::Duration(5).sleep();
        else
            break;
    }

    std::cout << "Setting guided! Z = "<<tmp_pose.position.z << std::endl;
    guided_set_mode.request.custom_mode = "GUIDED";
    arm_cmd.request.value = true;
    set_mode_client.call(guided_set_mode);
    arming_client.call(arm_cmd);
    ros::Duration(5).sleep();
}

void droneLand(double z)
{
    land_request.request.altitude = 0;
    land_client.call(land_request);
    ros::Duration((short)(15+z)).sleep();
}

void droneTakeOff(double z)
{
    setGuidedMode();
    takeoff_request.request.altitude = z;
    takeoff_client.call(takeoff_request);
    ros::Duration((short)(15+z)).sleep();
}

void droneLandTakeOff(double previous_z, double z)
{
    droneLand(previous_z);
    droneTakeOff(z);
    sleep(2);
}

void drone_fly_to_position(XYZ_pos point)
{
  target_position.coordinate_frame = 1;
  target_position.position.x = point.x; // meters
  target_position.position.y = point.y; // meters
  target_position.position.z = point.z; // meters
  //prepocet atan2() na yaw
  double angle = atan2((point.y - tmp_pose.position.y), (point.x - tmp_pose.position.x));

  target_position.yaw = angle; //radians
  std::cout << "flying to pos X " << point.x << ", " << point.y << ", " << point.z << std::endl;
}

double getCurrentYaw() {
  double quatx = tmp_pose.orientation.x;
  double quaty = tmp_pose.orientation.y;
  double quatz = tmp_pose.orientation.z;
  double quatw = tmp_pose.orientation.w;

  tf::Quaternion q(quatx, quaty, quatz, quatw);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);

  return yaw;
}



int main(int argc, char **argv)
{ 
  double x = 5.0;
  double y = 0.0;
  double z = 1.0;

  // world y = drone x ; - world x = drone y 
  XYZ_pos target = { x, y, z };

  ros::init(argc, argv, "point_cloud_listener");

  ros::NodeHandle n;

  // pointcloud subscribers
  sub = n.subscribe("fei_lrs_drone/stereo_camera/points2", 1000, callback_pointcloud);

  // // navigation subscribers
  // arming_client = n.serviceClient<mavros_msgs::CommandBool> ("mavros/cmd/arming");
  // set_mode_client = n.serviceClient<mavros_msgs::SetMode> ("mavros/set_mode");
  // takeoff_client = n.serviceClient<mavros_msgs::CommandTOL> ("mavros/cmd/takeoff");
  // land_client = n.serviceClient<mavros_msgs::CommandTOL> ("mavros/cmd/land");
  // local_pose_publisher = n.advertise<mavros_msgs::PositionTarget> ("mavros/setpoint_raw/local", 10);

  // ros::AsyncSpinner spinner(0);
  // spinner.start();

  

  // //take off to first
  // droneTakeOff(target.z); 

  ros::Rate r(10); 
  while (cnt == 0)
  {
    ros::spinOnce();
    // {
    //   std::lock_guard<std::mutex> guard(mutex_);
    //   tmp_pose = pose;
    // }    

    // if (!OBSTACLE)
    // {
    //   drone_fly_to_position(target);
    // }
    // else 
    // {
    //   std::cout << "WAITING" << std::endl;
    //   XYZ_pos hold_pos { pose.position.x, pose.position.y, pose.position.z };
    //   drone_fly_to_position(hold_pos);
    // }
    // local_pose_publisher.publish(target_position);
    r.sleep();
  }

  return 0;
}
#include "ros/ros.h"
#include "nav_msgs/OccupancyGrid.h"

#include "nav_msgs/MapMetaData.h"

#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/MultiArrayLayout.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Pose.h"
#include <mavros_msgs/State.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/PositionTarget.h>
#include <memory>
#include <chrono>
#include <mutex>
#include <tf/transform_datatypes.h>

#define HARD_DISTANCE 0.15
#define SOFT_DISTANCE 0.25

// #define DEBUG 1

#ifdef DEBUG
// std::vector<double> TEST_DATA = {2.0, 8.600000381469727, 6.899999618530273, 8.65, 2.02,
//                                   3.0, 7.5, 2.0, 7.5, 5.30000114440918, 4.84, 5.37,
//                                   2.0, 2.0, 5.30000114440918, 2.08, 9.74,
//                                   3.0, 2.0, 7.700000762939453, 8.80000114440918, 7.700000762939453, 8.84, 6.9,
//                                   3.0, 7.600000381469727, 6.80000114440918, 7.600000381469727, 8.100000381469727, 2.81, 8.15,
//                                   3.0, 2.8000011444091797, 7.700000762939453, 12.899999618530273, 7.700000762939453, 13.0, 7.0};
std::vector<double> TEST_DATA = {3.0, 8.0, 6.899999618530273, 8.0, 8.899999618530273, 1.85, 9.0, 2.0, 1.8000011444091797, 9.700000762939453, 6.72, 9.71, 3.0, 6.700000762939453, 7.80000114440918, 8.899999618530273, 7.80000114440918, 8.96, 2.0, 3.0, 7.899999618530273, 1.8999996185302734, 7.899999618530273, 5.200000762939453, 2.55, 5.21, 3.0, 2.5, 5.700000762939453, 12.899999618530273, 5.700000762939453, 13.0, 7.0};
const unsigned int TEST_SIZE = 5;
#endif

enum TaskType 
{
  pass = 0,
  takeoff = 1,
  land = 2,
  landtakeoff = 3,
};

TaskType convert_to_ennum(const std::string& str)
{
    if(str == "-") return pass;
    else if(str == "takeoff") return takeoff;
    else if(str == "land") return land;
    else if(str == "landtakeoff") return landtakeoff;
};

const std::string convert_to_string(const TaskType& type)
{
    if(type == pass) return "[pass]";
    else if(type ==  takeoff) return "[takeoff]";
    else if(type == land) return "[land]";
    else if(type == landtakeoff) return "[landtakeoff]";
};

struct XYZ_pos
{
    double x;
    double y;
    double z;
};

struct Point 
{
  XYZ_pos target_point;
  std::vector<XYZ_pos> pass_throught_points;
  TaskType task;
  bool is_soft_precision;
};

ros::Subscriber points_sub, odom_sub, state_sub;
ros::ServiceClient set_mode_client, arming_client, takeoff_client, land_client;
ros::Publisher local_pose_publisher;
mavros_msgs::SetMode guided_set_mode;
mavros_msgs::State current_state;
mavros_msgs::CommandBool arm_cmd;
mavros_msgs::CommandTOL takeoff_request, land_request;
geometry_msgs::Pose pose;
geometry_msgs::Pose tmp_pose;
mavros_msgs::PositionTarget target_position;

// container for our missions points defined in config.yaml
std::vector<Point> mission_points;

std::vector<double> x_coords;
std::vector<double> y_coords; 
std::vector<double> z_coords;
std::vector<std::string> precision; 
std::vector<std::string> task; 

bool points_loaded = false;
bool initial_bool = false;
std::mutex mutex_;

void make_points(const std::vector<double>& data, unsigned int n_targets)
{
  std::cout << "Number of targets is: " << n_targets << std::endl;
  int index = 0;

  //Vlozenie prveho bodu do vectora vsetkych bodov (to bude len 'takeoff')
  XYZ_pos first_target = {0.0, 0.0, z_coords.at(0)};
  TaskType first_task = convert_to_ennum(task.at(0));
  bool first_precision = precision.at(0) == "soft" ? true : false;
  Point first_point = {first_target, {}, first_task, first_precision};
  mission_points.push_back(first_point);

  for (int i = 0; i < n_targets; i++)
  {
    // acquire pass throught points between targets
    unsigned int n_points = (unsigned int) data.at(index);
    std::vector<XYZ_pos> pass_points;
    XYZ_pos target_point;
    for (int j = index+1; j < index+1 + 2*(n_points); j += 2)
    {
      // Prepocet suradnic mapy do localnych suradnic robota
      double x_pos = 7 - data.at(j + 1); double y_pos = data.at(j) - 13; double z_pos = z_coords.at(i+1);

      if (j == index+1 + 2*(n_points-1))
        target_point = {x_pos, y_pos, z_pos};
      else
        pass_points.push_back({x_pos, y_pos, z_pos});
    }

    bool is_soft_precision = precision[i+1] == "soft" ? true : false;
    TaskType type = convert_to_ennum(task[i+1]);

    Point point( { target_point, pass_points, type, is_soft_precision } );
    mission_points.push_back(point);
    index += n_points*2 + 1;
  }

  for(int i = 1; i < mission_points.size(); i++)
  {

    XYZ_pos prev_target = mission_points.at(i-1).target_point;
    prev_target.z = mission_points.at(i).target_point.z;
    mission_points.at(i).pass_throught_points.insert(mission_points.at(i).pass_throught_points.begin(), prev_target);
  }

  for (int i = 0; i < mission_points.size(); i++)
  {
    std::cout << std::endl << "..............................................." << std::endl;
    std::cout << "[Target " << mission_points[i].target_point.x << ", " << mission_points[i].target_point.y <<  ", " << mission_points[i].target_point.z << "]: " << std::endl;
    std::cout << "Precious of target is soft: " << mission_points[i].is_soft_precision << std::endl;
    std::cout << "Task for target is: " << convert_to_string(mission_points[i].task) << std::endl;
    std::cout << "Number of passthrought points: " << mission_points[i].pass_throught_points.size() << std::endl;
    for (int j = 0; j < mission_points[i].pass_throught_points.size(); j++)
    {
      std::cout << "[Point " << mission_points[i].pass_throught_points[j].x << ", " << mission_points[i].pass_throught_points[j].y << ", " << mission_points[i].pass_throught_points[j].z << "]: " << std::endl;
    }
  }
}

void getPointsCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
  unsigned int size = msg->layout.dim[0].stride-1;
  std::vector<double> data_vector = msg->data;
  make_points(data_vector, size);

  points_loaded = true;
  points_sub.shutdown();
}

void getOdometryCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  {
    std::lock_guard<std::mutex> guard( mutex_ );
    pose = msg->pose.pose;
    initial_bool = true;
  }
}

void state_cb(const mavros_msgs::State::ConstPtr& msg)
{
    current_state = *msg;
}

void setGuidedMode()
{
    while (true) 
    {
        {
            std::lock_guard<std::mutex> guard(mutex_);
            tmp_pose = pose;
        }
        if(tmp_pose.position.z > 0.05)
            ros::Duration(5).sleep();
        else
            break;
    }

    std::cout << "Setting guided! Z = "<<tmp_pose.position.z << std::endl;
    guided_set_mode.request.custom_mode = "GUIDED";
    arm_cmd.request.value = true;
    set_mode_client.call(guided_set_mode);
    arming_client.call(arm_cmd);
    ros::Duration(5).sleep();
}

void droneLand(double z)
{
    land_request.request.altitude = 0;
    land_client.call(land_request);
    ros::Duration((short)(15+z)).sleep();
}

void droneTakeOff(double z)
{
    setGuidedMode();
    takeoff_request.request.altitude = z;
    takeoff_client.call(takeoff_request);
    ros::Duration((short)(15+z)).sleep();
}

void droneLandTakeOff(double previous_z, double z)
{
    droneLand(previous_z);
    droneTakeOff(z);
    sleep(2);
}

void drone_fly_to_position(XYZ_pos point)
{
  target_position.coordinate_frame = 1;
  target_position.position.x = point.x; // meters
  target_position.position.y = point.y; // meters
  target_position.position.z = point.z; // meters

  //prepocet atan2() na yaw
  double angle = atan2((point.y - tmp_pose.position.y), (point.x - tmp_pose.position.x));

  target_position.yaw = angle; //radians
}

bool distance_control(XYZ_pos target, bool is_soft_precision)
{
    double safe_dist = sqrt(pow((tmp_pose.position.x - target.x), 2) + pow((tmp_pose.position.y - target.y), 2) + pow((tmp_pose.position.z - target.z), 2));
    if (!is_soft_precision && safe_dist < HARD_DISTANCE)
    {
        return true;
    }
    else if(is_soft_precision && safe_dist < SOFT_DISTANCE)
    {
        return true;
    }
    else
    {
      return false;
    }
}

double getCurrentYaw() {
  double quatx = tmp_pose.orientation.x;
  double quaty = tmp_pose.orientation.y;
  double quatz = tmp_pose.orientation.z;
  double quatw = tmp_pose.orientation.w;

  tf::Quaternion q(quatx, quaty, quatz, quatw);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);

  return yaw;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "map_planning");

  ros::NodeHandle n;

  //service clients
  state_sub = n.subscribe<mavros_msgs::State> ("mavros/state", 10, state_cb);
  arming_client = n.serviceClient<mavros_msgs::CommandBool> ("mavros/cmd/arming");
  set_mode_client = n.serviceClient<mavros_msgs::SetMode> ("mavros/set_mode");
  takeoff_client = n.serviceClient<mavros_msgs::CommandTOL> ("mavros/cmd/takeoff");
  land_client = n.serviceClient<mavros_msgs::CommandTOL> ("mavros/cmd/land");
  local_pose_publisher = n.advertise<mavros_msgs::PositionTarget> ("mavros/setpoint_raw/local", 10);

  n.getParam("/x_coordinates", x_coords);
  n.getParam("/y_coordinates", y_coords);
  n.getParam("/z_coordinates", z_coords);
  n.getParam("/precision", precision);
  n.getParam("/task", task);

  ros::Rate loop_rate(10);
#ifndef DEBUG
  points_sub = n.subscribe("trajectory_points", 10, getPointsCallback);
#else
  make_points(TEST_DATA, TEST_SIZE);
#endif
  odom_sub = n.subscribe("mavros/global_position/local", 10, getOdometryCallback);

  // unsigned target_index = 0;

  ros::AsyncSpinner spinner(0);
  spinner.start();
#ifndef DEBUG
  while(true)
  {
    {
      std::lock_guard<std::mutex> guard(mutex_);
      sleep(1);
      if(points_loaded) break;
      
    }
  }
#endif

  //take off to first
  droneTakeOff(z_coords[0]);  
  
  while(true)
  {
    {
      std::lock_guard<std::mutex> guard(mutex_);
      sleep(1);
      if(initial_bool) break;
      
    }
  }

  std::cout << "letim" << std::endl;

  int target_index = 0;
  int point_index = 0;
  Point current_target = mission_points.at(target_index); 
  float current_target_yaw = target_position.yaw;
  float new_x, new_y;
  bool is_rotating = false;

  while(ros::ok()) 
  {
    ros::spinOnce();
    {
      std::lock_guard<std::mutex> guard(mutex_);
      tmp_pose = pose;
      
    }

    //   TU sa ide kokooooooodit ========================
    if(target_index < mission_points.size())
    {
      bool point_reched = false;
      bool target_in_processing = false;

      
      XYZ_pos current_point;
      bool soft_precision;
      current_target = mission_points.at(target_index);

      if(point_index < current_target.pass_throught_points.size())
      {
        current_point = current_target.pass_throught_points.at(point_index);
        soft_precision = true;
      }
      else
      {
        current_point = current_target.target_point;
        soft_precision = current_target.is_soft_precision;
        target_in_processing = true;
      }

      point_reched = distance_control(current_point, soft_precision);

      if(point_reched)
      {
        point_index++;
        if(target_in_processing)
        {
          target_index++;
          point_index = 0;
          
          switch(current_target.task)
          {
            case pass:
            {
              current_target = mission_points.at(target_index);
              current_point = current_target.pass_throught_points.at(point_index);
              drone_fly_to_position(current_point);
              std::cout << "============= PASS" << std::endl;
              std::cout << "Idem do Bodu: [x,y,z]: " << current_point.x << ", " << current_point.y << ", " << current_point.z << ", " << std::endl;
              break;
            }

            case takeoff:
            {
              current_target = mission_points.at(target_index);
              current_point = current_target.pass_throught_points.at(point_index);
              drone_fly_to_position(current_point);
              std::cout << "============== TAKING OFF" << std::endl;
              std::cout << "Idem do Bodu: [x,y,z]: " << current_point.x << ", " << current_point.y << ", " << current_point.z << ", " << std::endl;
              // droneTakeOff(current_point.z);
              break;
            }

            case land:
            {
              droneLand(current_point.z);
              break;
            }

            case landtakeoff:
            {
                Point temp = mission_points.at(target_index);
              droneLandTakeOff(current_point.z, temp.target_point.z);
              break;
            }
          }
        }
        else
        {
          if(point_index >= current_target.pass_throught_points.size())
          {
            current_point = current_target.target_point;
            point_index = 0;
          }
          else
          {
            current_point = current_target.pass_throught_points.at(point_index);
          }
          drone_fly_to_position(current_point);
        }
        
      }
    }

    if (target_position.yaw != current_target_yaw) { // prisla zmena zelaneho otocenia
      current_target_yaw = target_position.yaw;
      new_x = target_position.position.x;
      new_y = target_position.position.y;
      target_position.position.x = tmp_pose.position.x;
      target_position.position.y = tmp_pose.position.y;
      is_rotating = true;
    } else if (is_rotating && abs(current_target_yaw - getCurrentYaw()) < 0.1) { // toci sa
      target_position.position.x = new_x;
      target_position.position.y = new_y;
      is_rotating = false;
    }

    local_pose_publisher.publish(target_position);
    loop_rate.sleep();
  }

  return 0;

}
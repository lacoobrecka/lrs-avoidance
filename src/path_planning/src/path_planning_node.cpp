#include "ros/ros.h"
#include "nav_msgs/OccupancyGrid.h"

#include "nav_msgs/MapMetaData.h"

#include "std_msgs/Float64MultiArray.h"
#include "std_msgs/MultiArrayLayout.h"

#define NORTH 1
#define EAST 2
#define SOUTH 3
#define WEST 4


typedef struct
{
    double x;
    double y;
}X_Y;

typedef struct
{
    int i;
    int j;
}IJ_idx;

bool start_sending = false;
std_msgs::Float64MultiArray points_msg;
char number_of_points;
ros::Subscriber map_sub;
short grid_max_width_idx;
short grid_max_height_idx;
// short **map;
// std::vector<std::int16_t> all_points;
std::vector<double> all_points;
// ros::NodeHandle n;
std::vector<double> x_coords;
std::vector<double> y_coords; 

// short eval_map;
// std::vector<std::pair<unsigned short>> targets;

int getDirection(IJ_idx pos, IJ_idx new_pos){
  if(new_pos.i < pos.i) return NORTH;
  if(new_pos.j > pos.j) return EAST;
  if(new_pos.i > pos.i) return SOUTH;
  if(new_pos.j < pos.j) return WEST;
  return 0;
}

void evaluateMapFromGoal(short **map, IJ_idx idxs_tar){
  *(*(map + idxs_tar.i) + idxs_tar.j) = 2;
  std::vector<IJ_idx> centralPlaces;
  centralPlaces.push_back(idxs_tar);
  while(!centralPlaces.empty()){
    IJ_idx places = centralPlaces.at(0);

    for(int ii = -1; ii <= 1; ii++){
      for(int jj = -1; jj <= 1; jj++){
        if(ii != 0 && jj != 0){
          continue; // stvorsusednost
        }
        if(*(*(map + (places.i+ii)) + (places.j+jj)) != 0){
          continue; // prekazka alebo uz ohodnotene pole
        }
        if(places.i >= 0 && places.i < grid_max_width_idx && places.j >= 0 && places.j < grid_max_height_idx){
          *(*(map + (places.i+ii)) + (places.j+jj)) = *(*(map + places.i) + places.j) + 1;
          centralPlaces.push_back({places.i+ii, places.j+jj});
        }
      }
    }
    centralPlaces.erase(centralPlaces.begin());
  }
  
}

void planningTrajectory(short **map, IJ_idx pos_ij, X_Y target_coords_xy, nav_msgs::MapMetaData info){
  short i = pos_ij.i;
  short j = pos_ij.j;

  bool wrongDirection = true;
  bool shouldBreak = false;
  int direction = 0;
  std::vector<std::int16_t> generated_points;
  // std::vector<IJ_idx> allPlacesInTrajectory;
  // std::vector<IJ_idx> corners;
  // all_points.push_back(-1);

  while(*(*(map + i) + j) != 2){
    for(int ii = -1; ii <= 1; ii++){
      for(int jj = -1; jj <=1; jj++){
        if(ii != 0 && jj != 0) continue;
        if(ii == 0 && jj == 0) continue;

        if(wrongDirection){
          if(1 < *(*(map + (i+ii)) + (j+jj)) && *(*(map + (i+ii)) + (j+jj)) < *(*(map+i)+j)){
            direction = getDirection({i,j}, {i+ii, j+jj});
            wrongDirection = false;
            i += ii;
            j += jj;
            shouldBreak = true;
            break;
          }
        }else{
          bool bool_north = direction == NORTH && i+ii < i && j+jj == j;
          bool bool_east = direction == EAST && i+ii == i && j+jj > j;
          bool bool_south = direction == SOUTH && i+ii > i && j+jj == j;
          bool bool_west = direction == WEST && i+ii == i && j+jj < j;

          if(bool_north || bool_east || bool_south || bool_west){
            if(1 < map[i+ii][j+jj] && map[i+ii][j+jj] < map[i][j]){
              i += ii;
              j += jj;
            }else{
              wrongDirection = true;
              // corners.push_back({i, j});
              generated_points.push_back(i);
              generated_points.push_back(j);
            }
            shouldBreak = true;
            break;
          }
        }
      }
      if(shouldBreak){
        shouldBreak = false;
        break;
      }
    }
  }
  // corners.push_back({i, j});

  generated_points.push_back(i);
  generated_points.push_back(j);

  //separator in our case:  -1
  // all_points.push_back(-1);
  all_points.push_back(generated_points.size()/2);
  for(int i = 0; i < generated_points.size()-2; i+=2){
    double x = generated_points.at(i) * info.resolution + info.origin.position.x;
    double y = generated_points.at(i+1) * info.resolution + info.origin.position.y;
    all_points.push_back(x);
    all_points.push_back(y);
  }
  all_points.push_back(target_coords_xy.x);
  all_points.push_back(target_coords_xy.y);


  // std::cout << "Pocet zastavok: " << corners.size() << std::endl;

  // for(IJ_idx idx : corners){
  //   std::cout << "Bod: " << std::endl;
  //   std::cout << "I: " << idx.i << "; J: "<< idx.j << std::endl;
  // }
}


void getMapCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
  nav_msgs::MapMetaData info = msg->info;
  // info.resolution = 0.1
  number_of_points = msg->data.at(0);
  // int start_pos_x = (13 - info.origin.position.x)/info.resolution;  //index
  // int start_pos_y = (7 - info.origin.position.y)/info.resolution;   //index
  grid_max_width_idx = info.width;
  grid_max_height_idx = info.height;

  std::cout<< "number_of_points" << number_of_points << std::endl;

  ROS_INFO("Got map %d %d", info.width, info.height);
  short **map;
  map = (short **)calloc(info.width, sizeof(short *));
  for (int w=0; w<info.width; w++){
    map[w] = (short *)calloc(info.height, sizeof(short));
  }

  //v tomto fore naplnit mapu kt chceme publikovat
  for(int p = 1; p < number_of_points; p++) {
    for (unsigned int x = 0; x < info.width; x++) {
      for (unsigned int y = 0; y < info.height; y++) {
        
        // map[x][y] = msg->data[(p * info.width * info.height) + (x + info.width * y) + 1];
        *(*(map + x) + y) = msg->data[(p * info.width * info.height) + (x + info.width * y) + 1];
        // std::cout << map[x][y] << ", ";
      }
      // std::cout << std::endl;
    }
    std::cout << "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"<< std::endl;
    int target_pos_x = (x_coords.at(p) - info.origin.position.x)/info.resolution;
    int target_pos_y = (y_coords.at(p) - info.origin.position.y)/info.resolution;
    X_Y target_coords_xy = {x_coords.at(p), y_coords.at(p)};
    IJ_idx target_ij = {target_pos_x, target_pos_y};

    int current_pos_x = (x_coords.at(p-1) - info.origin.position.x)/info.resolution;
    int current_pos_y = (y_coords.at(p-1) - info.origin.position.y)/info.resolution;
    IJ_idx curr_pos_ij = {current_pos_x, current_pos_y};
    // IJ_idx target_ij = {190, 269};
    // IJ_idx curr_pos_ij = {311, 251};

    evaluateMapFromGoal(map, target_ij);
    for (unsigned int x = 0; x < info.width; x++) {
      for (unsigned int y = 0; y < info.height; y++) {
        std::cout << *(*(map + x) + y) << ", ";
      }
      std::cout << std::endl;
    }
    std::cout << "=========================================================================================================================================================================================================================================================="<< std::endl;

    planningTrajectory(map, curr_pos_ij, target_coords_xy, info);
  }
  for(int w = 0; w < info.width; w++){
    free(map[w]);
  }
  free(map);
  map_sub.shutdown();

  std_msgs::MultiArrayDimension msg_dim;
  msg_dim.label = "waypoints"; 
  msg_dim.size = all_points.size();
  msg_dim.stride = number_of_points;

  points_msg.layout.dim.clear();
  points_msg.layout.dim.push_back(msg_dim);
  points_msg.data = all_points;
  start_sending = true;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "map_planning");


  ros::NodeHandle n;
  
  n.getParam("/x_coordinates", x_coords);
  n.getParam("/y_coordinates", y_coords);

  // std::cout << "Ahojjdfgdfvdf" << std::endl;
  // std::cout << x_coords.size() << std::endl;
  // std::cout << "Co je tu:  " << x_coords.at(0) << x_coords.at(1) << std::endl;
  // std::cout << y_coords.at(0) << y_coords.at(1) << std::endl;
  // return 0;
  
  ros::Rate loop_rate(10);
  map_sub = n.subscribe("map_with_inflation",10,getMapCallback);

  ros::Publisher points_publisher = n.advertise<std_msgs::Float64MultiArray>("trajectory_points", 10);

  while (ros::ok())
  {
    ros::spinOnce();
    
    if(start_sending) {
      points_publisher.publish(points_msg);
    }

    loop_rate.sleep();   
  }
  return 0;
}
